import re
import requests
import json
import logging

class ChatParseException(Exception):
  pass

class ChatParse(object):
  def __init__(self, chatMsg = ''):
    self._chatMsg = chatMsg
    self._mentions = []
    self._emoticons = []
    self._urls = {}

  # this assumes the only thing this object does is make a JSON string,
  # but we'll go with that for now
  def __str__(self):
    self.parse(self._chatMsg)
    return self.makeJson()

  def parse(self, msg):
    ''' Parse HipChat tokens from chat message '''
    try:
      # parse mentions
      logging.debug('parse: ' + msg)
      pattern = re.compile(r'@([\w]+)')

      logging.debug('parse: find mentions')
      for mention in pattern.findall(msg):
        logging.debug('parse: mention: ' + mention)
        self._mentions.append(mention)

      # parse emoticons
      logging.debug('parse: find emoticons')
      pattern = re.compile(r'\(([a-zA-Z]{1,15})\)')
      for emoticon in pattern.findall(msg):
        self._emoticons.append(emoticon)

      # parse URLs
      logging.debug('parse: find URLs')
      pattern = re.compile(r'http[s]?://(?:[a-zA-Z0-9\-\.]+)\.(?:[a-zA-Z0-9]{2,3})(?:/[a-zA-Z0-9]*)*')
      for url in pattern.findall(msg):
        title = self.pageTitle(url)
        self._urls[url] = title
      logging.debug('parse: done with parse')
    except Exception as e:
      raise ChatParseException('ChatParse::parse (' + e.message + ')')

  def pageTitle(self, url):
    ''' Take URL and get page title from site '''
    logging.debug('pageTitle: ' + url)
    try:
      res = requests.get(url)
      if res.status_code != 200:
        logging.warning('Bad return code from requests URL: ' + url + ' status code:' + `res.status_code`)
        return None

      pattern = re.compile(r'.*?<title>(.*?)</title>.*')
      match = pattern.search(res.text)
      if match != None:
        s = match.group(1).replace('&quot','\"')
        s = s.replace('&amp','&')
        s = s.replace('&gt','>')
        s = s.replace('&lt','<')
        return s
      else:
        return None
    except Exception as e:
      raise ChatParseException('ChatParse::pageTitle (' + e.message + ')')

  def reset(self):
    ''' reset containers to empty '''
    del self._mentions [:]
    del self._emoticons [:]
    self._urls.clear();

  def tokenCount(self):
    ''' return number of tokens parsed from chat message '''
    return (len(self._mentions)  +
           len(self._emoticons)  +
           len(self._urls))

  # If the creation of JSON varies in format frequenty, this function
  # might be moved external to the ChatParse class and attached to an object instance
  # as a user defined funtion
  def makeJson(self):
    ''' convert tokens to JSON '''
    logging.debug('makeJson')
    try:
      if self.tokenCount():
        myjson = {}
        if len(self._mentions):
          myjson['mentions'] = self._mentions

        if len(self._emoticons):
          myjson['emoticons'] = self._emoticons

        if len(self._urls):
          linkarray = []
          for url in self._urls:
            link = {}
            link['url'] = url
            link['title'] = self._urls[url]
            linkarray.append(link)
            myjson['links'] = linkarray
        return json.dumps(myjson)
      else:
        return ""
    except Exception as e:
      raise ChatParseException('ChatParse::makeJson exception (' + e.message + ')')
    
# debug
def log(cp):
  logging.debug('mentions')
  logging.debug(cp._mentions)

  logging.debug('emoticons')
  logging.debug(cp._emoticons)

  logging.debug('urls')
  logging.debug(cp._urls)

