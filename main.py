
import sys
from ChatParse import ChatParse
from ChatParse import ChatParseException
from ChatParse import log
import logging


def main():
  """test driver for ChatParse class"""
  if len(sys.argv) > 1:
    for arg in sys.argv:
      if arg == 'debug':
        logging.basicConfig(level=logging.DEBUG)
  try: 
    teststrings = ( 
      '@chris you around?', 
      'Good morning! (megusta) (coffee)',
      'Olympics are starting soon; http://www.nbcolympics.com',
      '@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016',
      'Here is some text @rcj should like this, so should @maj and @raj check out http://www.example.com (mysmiley) (itsfriday)',
      'http://this is a bad URL/foo/bar',
      'http://www.foo.com/foo/bar')
    cp = ChatParse()
    for s in teststrings:
      cp.parse(s)
      log(cp) 
      j = cp.makeJson()
      logging.debug(j)
      cp.reset()

    # test out __repr__
    bad =   'http://www.foo.com/foo/bar'
    cp = ChatParse(bad)
    log(cp)
    cp = ChatParse(teststrings[0]);
    log(cp)
    cp = ChatParse()
    logging.debug('empty')
  except ChatParseException as e:
    logging.warning(e.message)

if __name__ == "__main__":
  main();

